const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {
  try{
    console.log(req.body.token);
    jwt.verify(req.body.token, process.env.JWT_KEY, (err, decoded)=> {
      if(err) {
        return res.status(401).json({
          message: "Error token"
        });
      } else {
        req.decoded = decoded;
        next();
      }
    });
    
  } catch (error) {
    return res.status(401).json({
      message: "Auth faileddddd"
    })
  }
}