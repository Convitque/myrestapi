const express = require('express');
const route = express.Router();
const mongoose = require("mongoose");
const User = require("../models/user");
const bcrypt = require("bcrypt")
const jwt = require('jsonwebtoken')
const checkAuth = require("../middleware/check-auth")

route.post('/signup', (req, res) => {
  if (!req.body.password || !req.body.email) {
    return res.status(409).json({
      message: "require email and password"
    })
  }
  User.find({
      email: req.body.email
    })
    .exec()
    .then(user => {
      if (user.length >= 1) {
        return res.status(409).json({
          message: "Mail exists"
        });
      } else {
        if (req.body.password) {
          bcrypt.hash(req.body.password, 10, (err, hash) => {
            if (err) {
              return res.status(500).json({
                error: err
              });
            } else {
              const user = new User({
                _id: new mongoose.Types.ObjectId(),
                email: req.body.email,
                password: hash
              });
              user
                .save()
                .then(result => {
                  console.log(result);
                  res.status(201).json({
                    message: "User created"
                  });
                  console.log(user)
                })
            }
          })
        }
      }
    })
    .catch(err => {
      res.status(500).json({
        error: err
      });
    });
});

route.post('/login', (req, res) => {
  User.find({
      email: req.body.email
    })
    .exec()
    .then(user => {
      if (user.length < 0) {
        return res.status(401).json({
          message: "Auth failed"
        });
      }
      bcrypt.compare(req.body.password, user[0].password, (err, result) => {
        if (err) {
          return res.status(401).json({
            message: "Auth failed"
          });
        }
        if (result) {
          const token = jwt.sign({
              email: user[0].email,
              userID: user[0]._id
            },
            process.env.JWT_KEY, {
              expiresIn: "1h"
            })

          return res.status(200).json({
            message: "Auth successful",
            token: token
          });
        }
        return res.status(401).json({
          message: "Auth failed"
        });
      })

    })
    .catch(err => {
      res.status(500) / json({
        error: err
      });
    })
});

route.delete('/:userID', checkAuth, (req, res) => {
  const id = req.params.userID;
  User.remove({
      _id: id
    })
    .exec()
    .then(result => {
      res.status(200).json({
        message: "User deleted",
        request: {
          type: "DELETE",

        }
      });
    })
    .catch(err => {
      res.status(500).json({
        error: err
      });
    })
});

module.exports = route;